package ru.tsc.anaumova.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.anaumova.tm.dto.model.ProjectDto;

import java.util.List;

@Getter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractResponse {

    @Nullable
    private List<ProjectDto> projects;

    public ProjectListResponse(@Nullable final List<ProjectDto> projects) {
        this.projects = projects;
    }

}