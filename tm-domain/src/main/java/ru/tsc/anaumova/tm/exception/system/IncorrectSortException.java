package ru.tsc.anaumova.tm.exception.system;

import ru.tsc.anaumova.tm.exception.AbstractException;

public final class IncorrectSortException extends AbstractException {

    public IncorrectSortException(final String value) {
        super("Error! Incorrect sort. Value `" + value + "` not found...");
    }

}