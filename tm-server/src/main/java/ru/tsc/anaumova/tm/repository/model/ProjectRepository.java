package ru.tsc.anaumova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @NotNull
    List<Project> findAllByUserId(@NotNull String userId);

    @Nullable
    Project findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteAllByUserId(@NotNull String userId);

}