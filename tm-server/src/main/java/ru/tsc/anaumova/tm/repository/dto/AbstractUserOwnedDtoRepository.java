package ru.tsc.anaumova.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.anaumova.tm.dto.model.AbstractUserOwnedModelDto;

@Repository
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends AbstractDtoRepository<M> {

}